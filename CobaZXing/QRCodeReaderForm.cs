﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace CobaZXing
{
    public partial class QRCodeReaderForm : Form
    {
        public QRCodeReaderForm()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                    var reader = new BarcodeReader()
                    {
                        AutoRotate = true,
                        TryInverted = true
                    };
                    Result result = reader.Decode((Bitmap)pictureBox1.Image);
                    txtResult.Text = result.ToString().Trim();
                }
                catch
                {
                    MessageBox.Show("Oops! Sorry.\nSomething Went Wrong.",
                                Application.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                }
            }
        }
    }
}
