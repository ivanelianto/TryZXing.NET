﻿namespace CobaZXing
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerateQRCode = new System.Windows.Forms.Button();
            this.btnReadQRCode = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGenerateQRCode
            // 
            this.btnGenerateQRCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateQRCode.Location = new System.Drawing.Point(108, 125);
            this.btnGenerateQRCode.Name = "btnGenerateQRCode";
            this.btnGenerateQRCode.Size = new System.Drawing.Size(150, 75);
            this.btnGenerateQRCode.TabIndex = 0;
            this.btnGenerateQRCode.Text = "Generate QR Code";
            this.btnGenerateQRCode.UseVisualStyleBackColor = true;
            this.btnGenerateQRCode.Click += new System.EventHandler(this.btnGenerateQRCode_Click);
            // 
            // btnReadQRCode
            // 
            this.btnReadQRCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadQRCode.Location = new System.Drawing.Point(264, 125);
            this.btnReadQRCode.Name = "btnReadQRCode";
            this.btnReadQRCode.Size = new System.Drawing.Size(150, 75);
            this.btnReadQRCode.TabIndex = 1;
            this.btnReadQRCode.Text = "Read QR Code";
            this.btnReadQRCode.UseVisualStyleBackColor = true;
            this.btnReadQRCode.Click += new System.EventHandler(this.btnReadQRCode_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 325);
            this.Controls.Add(this.btnReadQRCode);
            this.Controls.Add(this.btnGenerateQRCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HomeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGenerateQRCode;
        private System.Windows.Forms.Button btnReadQRCode;
    }
}