﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace CobaZXing
{
    public partial class QRCodeGeneratorForm : Form
    {
        BarcodeWriter writer;

        public QRCodeGeneratorForm()
        {
            InitializeComponent();

            QrCodeEncodingOptions options = new QrCodeEncodingOptions()
            {
                DisableECI = true,
                CharacterSet = "UTF-8",
                Width = 250,
                Height = 250
            };
            writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options = options;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (rtbText.Text.Trim().Length != 0)
            {
                Bitmap result = new Bitmap(writer.Write(rtbText.Text));
                imgBarcode.Image = result;
            }
            else
            {
                MessageBox.Show("Text To Be Generate Cannot Be Empty.",
                                Application.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Image Files|*.png";
            saveDialog.DefaultExt = "png";
            saveDialog.AddExtension = true;
            saveDialog.FileName = "QRCode";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                ((Bitmap)imgBarcode.Image).Save(saveDialog.FileName);
                MessageBox.Show("Image Saved Successfully!",
                                Application.ProductName,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.None);
            }
        }
    }
}
